# GameDog Public Issue Tracking Repository
GameDog is supposed to be a WatchDog application for Games like CS:GO and others to display Gameinformations outside of the Game especially using a Chroma Keyboard. Right now it is focusing on CS:GO but Dota support is already included in a limited state (more coming soon!).

This Repository is for tracking issues and providing informations and help.

## Version
2.0

# Index
1. [Usage](#usage)
2. [Customization](#customization)
3. [FAQ](#faq)


# Usage
1. Download latest Version from the Razer Workshop or the GameDog Thread.
2. Install GameDog
3. Open Synapse and make sure Chroma Apps are enabled under the "Chroma Apps -> Settings" Tab
4. Start GameDog
5. Make sure your Chroma Devices play a Green Animation and then return back to your default Profile.
6. Make sure under Settings in GameDog the Directory to your Game is set correctly.
7. Minimize GameDog. It will stay open in the Tray Bar.
7. Start your Game and GameDog should receive Data and change the lighting accordingly.
8. After playing GameDog will revert the Keyboard back to your Synapse Profile

# Customization
Keys can be assigned to the following "Functions":

### Static Color  
Displayed in one color all the time

### Two Colors  
Displays a Game Value in a range from 0 to 100% (or true/false as 0 and 100%) and fades between two Colors.  
If the percent drops below the "Flash below percent" the Key or Mouse(pad) side will start flashing either in the Color defined via Flash Color or if that one is transparent in the Color defined via Color Start and Color End. Every Game Value that does not include "Percent" is displayed either as 0% or as 100% depending on the true/false state of the Value.

### Count Down Colors  
Same as Two Colors but makes the Assigned Keys or Mouse(pad) sides light up in the defined order depending on the percentage of the Game Value.

### (CSGO only) Effect: x  
Makes the whole Keyboard light up for an Effect, White for flash, Grey for smoke and red for Burning

### (CSGO only) Kill Counter  
Sets Keys one after another for each Kill the player did this round.  
This means if there are 5 Keys assigned and the player got 2 Headshots and one normal Kill, then 2 of the 5 Keys will light up in the HeadshotColor, one will light up in the KillColor one one will light up in the NoneColor

### (CSGO only) Bomb  
Fades Keys from ColorStart to ColorEnd within until 10 Seconds are left.  
Then switches to ColorDefusable (defusable with Def-Kit) for 5 Seconds before going to ColorNonDefusable

### (CSGO only) Roundphase  
Displays Freezetime (FreezetimeColor), Live (PlayingColor) or unknown (UndefinedColor) state of the current round on Keys

### Menu and Typing  
Lights up the whole Keyboard depending if the user is Typing or in Menu


# FAQ

## - GameDog shows "Chroma SDK not enabled or available." or similar.

* Open Synapse, Select a Chroma Device and click on the Tab "Chroma Apps".  
Make sure you have "Chroma Apps" enabled there:  
![Image of Yaktocat](https://i.gyazo.com/6070ea942469697083943127da0a2380.png)

* Open Taskmanager and make sure the "Razer Chroma SDK Service" is running.


## - GameDog does not change to yellow once started.
**Note: Since Version 2.0 GameDog does NOT change to yellow anymore.  
 Make sure that once you start GameDog a green loading animation is playing.  
 After that GameDog will give back control to Synapse until you start a supported Game.  
 If the Green animation does not play follow the following steps** 

* Open Synapse and go to Chroma Apps and to the Apps List and make sure GameDog is not set as deactivated.
* Restart your computer as sometimes the SDK can get stuck.
* Sometimes as a workaround turn off Synapse (rightclick in the Taskbar) and try again.

## - GameDog acts weird, wrong Keys light up or no reaction at all
In some cases an Update to Synapse can cause issues like that.  
This can be fixed by uninstalling Synapse AND the SDK from "Programs and Features" of Windows and downloading and installing latest Version from the Razer Website:  
http://www.razerzone.com/synapse/

## - The menu is not showing up at the top

* Check your Firewall and Antivirus as GameDog needs to open a local Port

## - GameDog does not change to the defined Colors once the Game is started.

* Make sure to start GameDog BEFORE you start CS:GO or Dota2
* Go to the Settings Tab and make sure the Path for your Game is set correctly.  
It needs to be changed if the game is placed at another location.  
Make sure to set it to the main Game Folder not the bin Directory!
A correct Path looks like that:
CS:GO: G:\Steam\SteamApps\common\Counter-Strike Global Offensive
DOTA2: G:\Steam\SteamApps\common\dota 2 beta
* CSGO: After starting GameDog make sure a config file is being created under [YOUR CS:GO PATH]\csgo\cfg called gamestate_integration_gamedog.cfg
* DOTA: After starting GameDog make sure a config file is being created under [YOUR DOTA2 PATH]\game\dota\cfg\gamestate_integration called gamestate_integration_gamedog.cfg
* Check your Firewall and Antivirus as GameDog needs to open a local Port

## - GameDog doesn't show any Games.

* Go to the Settings Tab and make sure the Path for your Game is set correctly.  
It needs to be changed if the game is placed at another location.  
Make sure to set it to the main Game Folder not the bin Directory!
A correct Path looks like that:
CS:GO: G:\Steam\SteamApps\common\Counter-Strike Global Offensive
DOTA2: G:\Steam\SteamApps\common\dota 2 beta

